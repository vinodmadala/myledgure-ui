import { Injectable } from '@angular/core';
import {User} from '../app/models/user.model';
import {Router} from '@angular/router';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  readonly rootUrl = 'http://localhost:3000';
  readonly reqHeader = new HttpHeaders({ 'Content-Type': 'application/json', 'No-Auth': 'True' });
  constructor(private router: Router, private http: HttpClient) { }

  registerUser(userData) {
    return this.http.post(this.rootUrl + '/api/user/register', userData);
  }
}
