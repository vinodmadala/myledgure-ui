import {Component } from '@angular/core';
import { navItems } from '../../_nav';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './layout.component.html'
})
export class LayoutComponent {
  public sidebarMinimized = false;
  public navItems = navItems;
  constructor(private authenticationService: AuthenticationService) {
  }
  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  userLogout() {
    this.authenticationService.logout();
  }
}
