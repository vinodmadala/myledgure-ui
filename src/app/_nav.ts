import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer'
  },
  {
    name: 'Charts',
    url: '/charts',
    icon: 'icon-pie-chart'
  },
  {
    divider: true
  },
  {
    name: 'Error 404',
    url: '/404',
    icon: 'icon-star'
  },
  {
    name: 'Error 500',
    url: '/500',
    icon: 'icon-star'
  }
];
