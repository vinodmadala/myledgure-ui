export class User {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  mobileNumber: string;
  password: string;
  gender: string;
  dateOfBirth: string;
  authToken?: string;
}
