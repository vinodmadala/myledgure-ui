import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {BsDatepickerConfig} from 'ngx-bootstrap/datepicker';
import {ToastrService} from 'ngx-toastr';
import { Router } from '@angular/router';
import {UserService} from '../../user.service';
// import custom validator to validate that password and confirm password fields match
import { MustMatch } from '../../_helpers/confirm-password.validator';
import { AuthService } from 'angularx-social-login';
// Import angular social login providers
import { FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html'
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  datepickerConfig: Partial<BsDatepickerConfig>;

  private user: {};
  private loggedIn: boolean;

  constructor(private userService: UserService, private toastr: ToastrService, private router: Router,
              private formBuilder: FormBuilder,
              private socialAuthService: AuthService, private authenticationService: AuthenticationService) {
    this.datepickerConfig = Object.assign({}, {
      containerClass: 'theme-dark-blue',
      showWeekNumbers: true,
      // minDate: new Date(1960, 0,1),
      dateInputFormat: 'DD/MM/YYYY'

    });
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      // tslint:disable-next-line:max-line-length
      mobileNumber: ['', [Validators.required, Validators.pattern('^(?:(?:\\+|0{0,2})91(\\s*[\\ -]\\s*)?|[0]?)?[789]\\d{9}|(\\d[ -]?){10}\\d$')]],
      email: ['', [Validators.required, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$')]],
      password: ['', [Validators.required, Validators.pattern('^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$')]],
      confirmPassword: ['', [Validators.required, Validators.pattern('^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$')]],
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });

    this.socialAuthService.authState.subscribe((user) => {
      this.user = user;
      console.log(JSON.stringify(this.user));
      this.loggedIn = (user != null);
    });

  }
  get f() {
    return this.registerForm.controls;
  }

  signup() {
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    console.log('----------registeration form data--------');
    const registerData = {
      firstName : this.f.firstName.value,
      lastName : this.f.lastName.value,
      email : this.f.email.value,
      mobileNumber : this.f.mobileNumber.value,
      password : this.f.password.value,
    };

    console.log(JSON.stringify(registerData));

    this.userService.registerUser(registerData)
      .subscribe((data: any) => {
          if (data.Succeeded === true) {
            this.toastr.success('User registration successful');
            this.router.navigate(['/authentication/login']);
          } else {
            this.toastr.error('User already existed with that email!');
          }
        }, (error: any) =>
          console.log(error)
      );
  }

  signInWithGoogle(): void {
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID).then(userData => {
      if (userData) {
        this.authenticationService.signInWithGoogle(userData).subscribe((data: any) => {
          if (data.status === 200) {
              this.toastr.success('Logined Successfully!');
              this.router.navigate(['/']);
            } else {
              this.toastr.error(data.error);
            }
          }, (error: any) => {
            console.log(error)
            this.toastr.error('Something Went Wrong');
        });
      }
    });
  }
  signInWithFB(): void {
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID).then(userData =>{
      if (userData) {
        console.log(JSON.stringify(userData));
      }
    });
  }
  signOut(): void {
    this.socialAuthService.signOut();
  }
}
